from FlaskApp import app
from flask import render_template

@app.route('/')
def index():
    user={'username': 'faoziaziz'}
    return render_template('index.html', title='Home', user=user)

@app.route('/test')
def test():
    return "tested"
